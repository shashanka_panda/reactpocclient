import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import { thArray, tdArray } from "variables/Variables.jsx";

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: []
    };

  }
  componentDidMount() {
    fetch('https://localhost:5001/api/product')
    .then(response => response.json())
    .then(data => this.setState({products: data}),  () => console.log(this.state.products));
  }
  render() {
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                ctTableFullWidth
                ctTableResponsive
                content={
                  <Table striped hover>
                    <thead>
                      <tr>
                        {thArray.map((prop, key) => {
                          return <th key={key}>{prop}</th>;
                        })}
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.products.map((prop, key) => {
                        return (
                          <tr key={key}>
                            {Object.keys(prop).map((item, key) => {
                              return item ==="imageUrl" ? <img style={{width:80, height:80, borderRadius:7}} 
      src={prop[item]}
      alt="No Image Available"
      />  : <td key={key}>{prop[item]}</td>;
                            })}
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Product;
